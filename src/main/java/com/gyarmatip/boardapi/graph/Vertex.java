package com.gyarmatip.boardapi.graph;

import com.gyarmatip.boardapi.navigation.Position;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents a vertex in a graph
 */
public class Vertex extends Position{

    private Map<Vertex, Integer> weightMap = new HashMap<>();

    public Vertex(int x, int y) {
        super(x, y);
    }

    public Vertex(Position position) {
        super(position);
    }

    public Map<Vertex, Integer> getWeightMap() {
        return weightMap;
    }

}

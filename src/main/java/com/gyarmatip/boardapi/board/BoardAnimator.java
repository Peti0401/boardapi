package com.gyarmatip.boardapi.board;

import animatefx.animation.AnimationFX;
import animatefx.animation.FlipInX;
import animatefx.animation.FlipInY;
import com.gyarmatip.boardapi.board.tile.Tile;
import com.gyarmatip.boardapi.navigation.Position;
import com.jfoenix.transitions.JFXFillTransition;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.Transition;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class BoardAnimator {

    private static ConcurrentHashMap<Tile, Transition> activeTileTransitionMap = new ConcurrentHashMap<>();

    /***************************************************************************
     * * Unanimated highlighting * *
     **************************************************************************/

    /**
     * Highlight a tile on a given position of the board
     * @param board target board
     * @param position position of the tile to highlight
     */
    public static void highlightTile(Board board, Position position) {
        Tile tile = board.getTile(position);
        tile.highlightOn();
    }

    /**
     * Highlight a tile on a given position of the board
     * @param board target board
     * @param positions positions of the tiles to highlight
     */
    public static void highlightTiles(Board board, Position... positions) {
        for (Position position : positions) {
            highlightTile(board, position);
        }
    }

    public static void highlightTiles(Board board, Collection<? extends Position> positions) {
        highlightTiles(board, positions.toArray(new Position[0]));
    }

    /**
     * Unhighlight a tile on a given position of the board
     * @param board target board
     * @param position position of the tile to highlight
     */
    public static void unhighlightTile(Board board, Position position) {
        Tile tile = board.getTile(position);
        tile.highlightOff();
    }

    /**
     * Unhighlight a tile on a given position of the board
     * @param board target board
     * @param positions positions of the tiles to highlight
     */
    public static void unhighlightTiles(Board board, Position... positions) {
        for (Position position : positions) {
            unhighlightTile(board, position);
        }
    }

    /**
     * Unhighlight a tile on a given position of the board
     * @param board target board
     * @param positions positions of the tiles to highlight
     */
    public static void unhighlightTiles(Board board, Collection<? extends Position> positions) {
        unhighlightTiles(board, positions.toArray(new Position[0]));
    }

    /***************************************************************************
     * * Animated highlighting * *
     **************************************************************************/

    /**
     * Starts a fade transition on the tile on the specified position.
     * @param board target board
     * @param blinkDuration duration of a single blink
     * @param position position of the tile to blink
     */
    public static void blinkTile(Board board, Duration blinkDuration, Position position) {
        Tile tile = board.getTile(position);
        FadeTransition fadeTransition = new FadeTransition(blinkDuration, tile.getHighlighter());
        fadeTransition.setAutoReverse(true);

        double opacity = tile.getHighlighter().getOpacity();
        fadeTransition.setFromValue(opacity);
        fadeTransition.setToValue(tile.isHighlightOn() ? 0.0 : 1.0);
        fadeTransition.setCycleCount(Integer.MAX_VALUE);

        // on animation stop, hide the highlighter again
        fadeTransition.statusProperty().addListener((observable, oldValue, newValue) ->
        {
            if (newValue == Animation.Status.STOPPED) {
                tile.highlightOff();
            }
        });

        activeTileTransitionMap.put(tile, fadeTransition);
        fadeTransition.play();
    }

    /**
     * Starts a fade transition on the tiles on the specified positions.
     * @param board target board
     * @param blinkDuration duration of a single blink
     * @param positions positions of the tiles to blink
     */
    public static void blinkTiles(Board board, Duration blinkDuration, Position... positions) {
        for (Position position: positions) {
            blinkTile(board, blinkDuration, position);
        }
    }

    /**
     * Starts a fade transition on the tiles on the specified positions.
     * @param board target board
     * @param blinkDuration duration of a single blink
     * @param positions positions of the tiles to blink
     */
    public static void blinkTiles(Board board, Duration blinkDuration, Collection<? extends Position> positions) {
        blinkTiles(board, blinkDuration, positions.toArray(new Position[0]));
    }

    /***************************************************************************
     * * Fill transition * *
     **************************************************************************/

    /**
     * Plays a JFXFillTransition on the tiles on the specified positions.
     * @param board target board
     * @param toColor target color
     * @param duration duration of a singly cycle
     * @param cycleCount number of cycles
     * @param position position of the tile to color
     */
    public static void colorTile(Board board, Color toColor, Duration duration, int cycleCount, Position position) {
        Tile tile = board.getTile(position);
        JFXFillTransition fillTransition = new JFXFillTransition(duration, tile);

        Color fromColor = ((Color) tile.getTileType().getBasePaint());
        fillTransition.setFromValue(fromColor);
        fillTransition.setToValue(toColor);
        fillTransition.setCycleCount(cycleCount);
        fillTransition.setAutoReverse(true);

        // hiding children from the stack
        List<Node> toHide = tile.getChildren().stream()
                .filter(node -> node.getOpacity() > 0)
                .collect(Collectors.toList());
        toHide.forEach(node -> node.setOpacity(0.0));

        // on animation stop, hide the highlighter again
        fillTransition.statusProperty().addListener((observable, oldValue, newValue) ->
        {
            if (newValue == Animation.Status.STOPPED) {
                toHide.forEach(node -> node.setOpacity(1.0));
            }
        });

        activeTileTransitionMap.put(tile, fillTransition);
        fillTransition.play();
    }

    /**
     * Plays a JFXFillTransition on the tiles on the specified positions.
     * @param board target board
     * @param toColor target color
     * @param duration duration of a singly cycle
     * @param cycleCount number of cycles
     * @param positions positions of the tiles to color
     */
    public static void colorTiles(Board board, Color toColor, Duration duration, int cycleCount, Position... positions) {
        for (Position position : positions) {
            colorTile(board, toColor, duration, cycleCount, position);
        }
    }

    /**
     * Plays a JFXFillTransition on the tiles on the specified positions.
     * @param board target board
     * @param toColor target color
     * @param duration duration of a singly cycle
     * @param cycleCount number of cycles
     * @param positions positions of the tiles to color
     */
    public static void colorTiles(Board board, Color toColor, Duration duration, int cycleCount, Collection<? extends Position> positions) {
        colorTiles(board, toColor, duration, cycleCount, positions.toArray(new Position[0]));
    }

    /***************************************************************************
     * * Path animation * *
     **************************************************************************/

    public static void animatePath(Board board, Duration duration, Position... positions) {
        List<Tile> tiles = board.getTiles(positions);
        // delays follow an arithmetic series such that:
        // u1 = 0, n = tiles.size(), Sn = duration
        double u1 = 0; // first element
        double n = tiles.size(); // number of terms in series
        double Sn = duration.toMillis(); // sum of series
        double d = (2/(n-1))*((Sn/2)-u1); // difference

        List<AnimationFX> animations = new ArrayList<>();
        for (int i = 0; i < tiles.size(); i++) {
            Tile tile = tiles.get(i);
            double ui = i*d;
            AnimationFX animationFX = i % 2 == 0 ? new FlipInX(tile.getHighlighter()) : new FlipInY(tile.getHighlighter());
            animationFX.setDelay(Duration.millis(ui));
            animations.add(animationFX);
        }
        animations.forEach(AnimationFX::play);
    }

    public static void animatePath(Board board, Duration duration, Collection<? extends Position> positions) {
        animatePath(board, duration, positions.toArray(new Position[0]));
    }

    /***************************************************************************
     * * Animation control * *
     **************************************************************************/

    /**
     * Ends the active transition for a tile on the specified position of the board
     * @param board target board
     * @param position position of the tile to make dormant
     */
    public static void stopTransition(Board board, Position position) {
        Tile searchedTile = board.getTile(position);
        if (activeTileTransitionMap.containsKey(searchedTile)) {
            activeTileTransitionMap.get(searchedTile).stop();
            activeTileTransitionMap.remove(searchedTile);
        }
    }

    /**
     * Ends the active transitions of the tiles on the specified positions of the board
     * @param board target board
     * @param positions positions of the tiles to make dormant
     */
    public static void stopTransitions(Board board, Position... positions) {
        for (Position position : positions) {
            stopTransition(board, position);
        }
    }

    /**
     * Ends the active transitions of the tiles on the specified positions of the board
     * @param board target board
     * @param positions positions of the tiles to make dormant
     */
    public static void stopTransitions(Board board, Collection<? extends Position> positions) {
        stopTransitions(board, positions.toArray(new Position[0]));
    }

    /**
     * Ends the active transitions of the tiles on all positions on the board
     * @param board target board
     */
    public static void stopTransitions(Board board) {
        stopTransitions(board, board.getPositions());
    }

    public static void clearBoard(Board board) {
        board.getTiles().forEach(tile ->
        {
            stopTransition(board, board.getPosition(tile));
            unhighlightTile(board, board.getPosition(tile));
            tile.clear();
        });
    }

}

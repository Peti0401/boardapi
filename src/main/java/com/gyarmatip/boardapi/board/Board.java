package com.gyarmatip.boardapi.board;

import com.gyarmatip.boardapi.board.tile.Tile;
import com.gyarmatip.boardapi.board.tile.TileType;
import com.gyarmatip.boardapi.navigation.Position;
import javafx.beans.binding.DoubleExpression;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * An actual grid representation of the board, with all the tiles visualized.
 * It requires a TileType[][] as backing data.
 */
public class Board extends GridPane {

    /**
     * The structure of the board
     */
    private BoardStructure boardStructure;

    /**
     * Creates a new instance of {@code Board}.
     * @param cellSize size of a cell
     * @param boardStructure structure of the board
     */
    public Board(DoubleExpression cellSize, BoardStructure boardStructure) {
        super();
        getStyleClass().setAll("board");

        this.boardStructure = boardStructure;
        int numRows = boardStructure.getNumberOfRows();
        int numColumns = boardStructure.getNumberOfColumns();

        // setting up rows
        for (int i = 0; i < numRows; i++) {
            RowConstraints row = new RowConstraints(0, 0, 0, Priority.NEVER, VPos.CENTER, true);
            row.prefHeightProperty().bind(cellSize);
            row.minHeightProperty().bind(cellSize);
            row.maxHeightProperty().bind(cellSize);
            getRowConstraints().add(row);
        }

        // setting up columns
        for (int i = 0; i < numColumns; i++) {
            ColumnConstraints column = new ColumnConstraints(0, 0, 0, Priority.NEVER, HPos.CENTER, true);
            column.prefWidthProperty().bind(cellSize);
            column.minWidthProperty().bind(cellSize);
            column.maxWidthProperty().bind(cellSize);
            getColumnConstraints().add(column);
        }

        // populating Board with Tile objects based on tileTypeGrid
        for (int rowIndex = 0; rowIndex < numRows; rowIndex++) {
            for (int columnIndex = 0; columnIndex < numColumns; columnIndex++) {
                TileType tileType = boardStructure.getTileTypeGrid()[rowIndex][columnIndex];
                Tile tile = new Tile(tileType, getColumnConstraints().get(0).prefWidthProperty());
                add(tile, columnIndex, rowIndex);
            }
        }

    }

    public Position[] getPositions() {
        Position[] positions = new Position[boardStructure.getNumberOfRows()*boardStructure.getNumberOfColumns()];
        int index = 0;
        for (int r = 0; r < boardStructure.getNumberOfRows(); r++) {
            for (int c = 0; c < boardStructure.getNumberOfColumns(); c++) {
                positions[index++] = new Position(c, r);
            }
        }
        return positions;
    }

    /**
     * @param tile the tile whose position is returned
     * @return the position of a tile
     */
    public Position getPosition(Tile tile) {
        return new Position(GridPane.getColumnIndex(tile), GridPane.getRowIndex(tile));
    }

    /**
     * @return each child of this Board which is a Tile
     */
    public List<Tile> getTiles() {
        return getChildren().stream()
                .filter(node -> node instanceof Tile)
                .map(node -> (Tile)node)
                .collect(Collectors.toList());
    }

    /**
     * @param positions positions pointing to Tiles
     * @return a list of tiles on the specified positions
     */
    public List<Tile> getTiles(Position... positions) {
        return Arrays.stream(positions)
                .map(this::getTile)
                .collect(Collectors.toList());
    }

    /**
     * @param positions positions pointing to Tiles
     * @return a list of tiles on the specified positions
     */
    public List<Tile> getTiles(Collection<? extends Position> positions) {
        return getTiles(positions.toArray(new Position[0]));
    }

    /**
     * Searches the Board for a Tile on a given position
     * @param position the position from which a Tile should be returned
     * @return the Tile from the supplied position or null if there is no Tile on the given Position
     */
    public Tile getTile(Position position) {
        List<Node> tiles = getTiles().stream()
                .filter(node -> GridPane.getColumnIndex(node) == position.getX() &&
                                GridPane.getRowIndex(node) == position.getY())
                .collect(Collectors.toList());
        return tiles.isEmpty() ? null : ((Tile) tiles.get(0));
    }

    public BoardStructure getBoardStructure() {
        return boardStructure;
    }

}

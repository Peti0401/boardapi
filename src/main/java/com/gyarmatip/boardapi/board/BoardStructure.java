package com.gyarmatip.boardapi.board;

import com.gyarmatip.boardapi.board.tile.TileType;
import com.gyarmatip.boardapi.utils.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BoardStructure {

    private TileType[][] tileTypeGrid;
    private TileType[] tileTypes;

    public BoardStructure(TileType[][] tileTypeGrid, TileType[] tileTypes) {
        this.tileTypeGrid = tileTypeGrid;
        this.tileTypes = tileTypes;
    }

    /**
     * Defines a board structure.
     * @param tileTypeGrid a 2D array of TileType objects representing the board's structure
     * @param tileSetEnumClass the class of the enum from which the TileType constants are supplied
     */
    public BoardStructure(TileType[][] tileTypeGrid, Class<? extends Enum<? extends TileType>> tileSetEnumClass) {
        this(tileTypeGrid, ((TileType[]) tileSetEnumClass.getEnumConstants()));
    }

    /**
     * Defines a board structure.
     * @param characterGrid 2D array of characters assigned to TileType instances
     * @param tileSetEnumClass the class of the enum from which the TileType constants are supplied
     */
    public BoardStructure(Character[][] characterGrid, Class<? extends Enum<? extends TileType>> tileSetEnumClass) {
        this(convertCharactersToTileTypes(characterGrid, ((TileType[]) tileSetEnumClass.getEnumConstants())), tileSetEnumClass);
    }

    /**
     * Defines a board structure.
     * @param characterGridTxtPath path to the txt file holding the character board
     * @param tileSetEnumClass the class of the enum from which the TileType constants are supplied
     */
    public BoardStructure(String characterGridTxtPath, Class<? extends Enum<? extends TileType>> tileSetEnumClass) {
        this(Utils.txtToCharMatrix(characterGridTxtPath), tileSetEnumClass);
    }

    public TileType[][] getTileTypeGrid() {
        return tileTypeGrid;
    }

    /**
     * Converts a Character[][] into a TileType[][].
     * @param tileTypeCharacters 2D array of characters assigned to TileType instances
     * @return a TileType[][]
     */
    private static TileType[][] convertCharactersToTileTypes(Character[][] tileTypeCharacters, TileType[] tileSetEnumConstants) {
        TileType[][] tileTypes = new TileType[tileTypeCharacters.length][tileTypeCharacters[0].length];
        Map<Character, List<TileType>> map = Arrays.stream(tileSetEnumConstants).collect(Collectors.groupingBy(TileType::getCharacter));
        for (int r = 0; r < tileTypes.length; r++) {
            for (int c = 0; c < tileTypes[0].length; c++) {
                // current char
                Character character = tileTypeCharacters[r][c];
                // one element list if character is assigned to a TileType, else null
                List<TileType> list = map.get(character);
                tileTypes[r][c] = list == null ? null : list.get(0);
            }
        }
        return tileTypes;
    }

    public int getNumberOfRows() {
        return tileTypeGrid.length;
    }

    public int getNumberOfColumns() {
        return tileTypeGrid[0].length;
    }

    /**
     * @return a 2D array of the character representation of the TileType objects constituting this Board
     */
    public Character[][] getCharacterGrid() {
        int numRows = getNumberOfRows();
        int numColumns = getNumberOfColumns();
        Character[][] characterGrid = new Character[numRows][numColumns];
        for (int r = 0; r < numRows; r++) {
            for (int c = 0; c < numColumns; c++) {
                characterGrid[r][c] = tileTypeGrid[r][c].getCharacter();
            }
        }
        return characterGrid;
    }

}

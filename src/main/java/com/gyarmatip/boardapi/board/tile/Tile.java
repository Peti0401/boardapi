package com.gyarmatip.boardapi.board.tile;

import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.beans.binding.DoubleExpression;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;

import java.io.FileInputStream;
import java.util.Collections;


/**
 * An item in a board, with attributes specified by its TileType variable
 */
public class Tile extends StackPane {

    //TODO enum with states (MARKED_START, MARKED_END, MARKED_SELECTED...etc mapping icons to each)

    private TileType tileType;
    private Rectangle baseRectangle;
    private ImageView imageView;
    private Label label;
    private Rectangle highlighter;
    private DoubleExpression cellSize;
    /**
     * Always the top layer, hence all event handlers should be attached to this node
     */
    private StackPane top;

    public Tile(Tile tile) {
        this(tile.getTileType(), tile.cellSize);
    }

    public Tile(Tile tile, DoubleExpression cellSize) {
        this(tile.getTileType(), cellSize);
    }

    /**
     * Creates a new instance of {@code Tile} with exact attributes predefined by tileType,
     * and with a uniform width and height of cellSize.
     * @param tileType attributes of this Tile
     * @param cellSize width and height of this Tile
     */
    public Tile(TileType tileType, DoubleExpression cellSize) {
        super();
        this.tileType = tileType;
        this.cellSize = cellSize;
        getStyleClass().setAll("board-tile");

        // binding dimensions to define a square
        prefWidthProperty().bind(cellSize);
        prefHeightProperty().bind(cellSize);
        minWidthProperty().bind(cellSize);
        minHeightProperty().bind(cellSize);
        maxWidthProperty().bind(cellSize);
        maxHeightProperty().bind(cellSize);

        /* adding children from the TileType (layer 1 is the bottom-most layer) */

        // layer 1: base shape
        baseRectangle = new Rectangle();
        baseRectangle.widthProperty().bind(prefWidthProperty());
        baseRectangle.heightProperty().bind(prefWidthProperty());
        baseRectangle.setFill(tileType.getBasePaint());
        getChildren().add(baseRectangle);

        // layer 2: ImageView if a valid URL is supplied
        try {
            Image image = new Image(new FileInputStream(tileType.getImageURL()));
            imageView = new ImageView(image);
            imageView.fitWidthProperty().bind(prefWidthProperty());
            imageView.fitHeightProperty().bind(prefWidthProperty());
            getChildren().add(imageView);
        } catch (Exception ignore) { }

        // layer 3: Label
        label = new Label(tileType.getLabelText());
        labelOff();
        getChildren().add(label);

        // layer 4: Highlighter
        highlighter = new Rectangle();
        highlighter.widthProperty().bind(prefWidthProperty());
        highlighter.heightProperty().bind(prefWidthProperty());
        highlighter.setFill(tileType.getHighlighterPaint());

        highlightOff();
        getChildren().add(highlighter);

        // layer 5: Top
        top = new StackPane();
        top.getStyleClass().setAll("board-tile-top");

        top.prefWidthProperty().bind(prefWidthProperty());
        top.prefHeightProperty().bind(prefWidthProperty());
        top.minWidthProperty().bind(prefWidthProperty());
        top.minHeightProperty().bind(prefWidthProperty());
        top.maxWidthProperty().bind(prefWidthProperty());
        top.maxHeightProperty().bind(prefWidthProperty());

        getChildren().add(top);


        // HARD CODE
        label.opacityProperty().bind(highlighter.opacityProperty());
    }

    /**
     * Sets the highlighter rectangle's opacity to 1.0
     */
    public void highlightOn() {
        highlighter.setOpacity(1.0);
    }

    /**
     * Sets the highlighter rectangle's opacity to 0.0
     */
    public void highlightOff() {
        highlighter.setOpacity(0.0);
    }

    /**
     * Indicates whether the highlighter rectangle's opacity is greater than 0
     * @return highlighter.getOpacity() > 0.0;
     */
    public boolean isHighlightOn() {
        return highlighter.getOpacity() > 0.0;
    }

    /**
     * Sets the label's opacity to 1.0
     */
    public void labelOn() {
        label.setOpacity(1.0);
    }

    /**
     * Sets the label's opacity to 1.0
     */
    public void labelOff() {
        label.setOpacity(0.0);
    }

    /**
     * Indicates whether the label's opacity is greater than 0
     * @return label.getOpacity() > 0.0;
     */
    public boolean isLabelOn() {
        return label.getOpacity() > 0.0;
    }

    public TileType getTileType() {
        return tileType;
    }

    public Rectangle getBaseRectangle() {
        return baseRectangle;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public Label getLabel() {
        return label;
    }

    public Rectangle getHighlighter() {
        return highlighter;
    }

    public StackPane getTop() {
        return top;
    }

    public void markAsStartTile() {
        MaterialDesignIconView icon = new MaterialDesignIconView(MaterialDesignIcon.PIN);
        icon.glyphSizeProperty().bind(prefWidthProperty().multiply(0.75));
        addChild(icon);
    }

    public void markAsEndTile() {
        MaterialDesignIconView icon = new MaterialDesignIconView(MaterialDesignIcon.FLAG_CHECKERED);
        icon.glyphSizeProperty().bind(prefWidthProperty().multiply(0.75));
        addChild(icon);
    }

    public void markAsSelected() {
        MaterialDesignIconView icon = new MaterialDesignIconView(MaterialDesignIcon.CHECKBOX_MARKED_CIRCLE_OUTLINE);
        icon.glyphSizeProperty().bind(prefWidthProperty().multiply(0.75));
        addChild(icon);
    }

    public boolean isMarked() {
        return getChildren().stream().anyMatch(node -> node instanceof MaterialDesignIconView);
    }

    public void removeMarkers() {
        getChildren().removeIf(node -> node instanceof MaterialDesignIconView);
    }

    public void addChild(Node node) {
        getChildren().add(node);
        ObservableList<Node> workingCollection = FXCollections.observableArrayList(getChildren());
        int lastIndex = getChildren().size() - 1;
        Collections.swap(workingCollection, lastIndex - 1, lastIndex);
        getChildren().setAll(workingCollection);
    }

    public void clear() {
        removeMarkers();
        label.setText("");
        highlighter.setOpacity(0.0);
    }

}

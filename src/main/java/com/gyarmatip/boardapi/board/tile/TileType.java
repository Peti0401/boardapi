package com.gyarmatip.boardapi.board.tile;

import javafx.scene.paint.Paint;

/**
 * This interface collects the attributes of a given Tile.
 */
public interface TileType {

    /**
     * @return the character representation of this TileType
     */
    Character getCharacter();

    /**
     * @return the base color of the shape representing a Tile of this TileType
     */
    Paint getBasePaint();

    /**
     * In case of using an Image instead of a Shape, this method can be used, else it can return null.
     * @return the path of the Image which will be embedded into an ImageView and will be the body of a Tile of this TileType
     */
    String getImageURL();

    /**
     * @return the text which will appear on a Tile of this TileType
     */
    String getLabelText();

    /**
     * @return the color which will be used to highlight a Tile of this TileType
     */
    Paint getHighlighterPaint();

}

package com.gyarmatip.boardapi;

import com.gyarmatip.boardapi.board.Board;
import com.gyarmatip.boardapi.board.BoardAnimator;
import com.gyarmatip.boardapi.board.BoardStructure;
import com.gyarmatip.boardapi.graph.Vertex;
import com.gyarmatip.boardapi.navigation.Position;
import com.gyarmatip.boardapi.navigation.pathfinding.DijkstrasPathFinder;
import com.gyarmatip.boardapi.tilesets.HighwayTile;
import com.gyarmatip.boardapi.tilesets.PacmanTile;
import javafx.application.Application;
import javafx.beans.binding.DoubleExpression;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

public class Main extends Application {

    public static Board pacmanBoard;
    public static DijkstrasPathFinder pathFinder_pacman;

    public static Board highwayBoard;
    public static DijkstrasPathFinder pathFinder_highway;

    private Slider slider = new Slider(10, 50, 20);
    private DoubleExpression cellSize = slider.valueProperty();

    public void start(Stage primaryStage) throws Exception {
        initVariables();
        VBox realRoot = new VBox(20);

        TabPane root = new TabPane();
        root.setPadding(new Insets(15));
        Tab pacmanTab = new Tab("Pacman", new ScrollPane(pacmanBoard));
        Tab highwayTab = new Tab("Highway", new ScrollPane(highwayBoard));
        root.getTabs().addAll(pacmanTab, highwayTab);

        Button start_animation = new Button("Start animation");
        start_animation.setOnAction(event ->
        {
            List<Vertex> vertexList = new ArrayList<>(pathFinder_pacman.getVertices());
            Vertex v1 = vertexList.get(10);
            Vertex v2 = vertexList.get(30);
            Position[] pos = pathFinder_pacman.findPath(v1, v2);

            BoardAnimator.animatePath(pacmanBoard, Duration.millis(125).multiply(pos.length), pos);

        });

        Button end_animation = new Button("End animation");
        end_animation.setOnAction(event ->
        {
            BoardAnimator.stopTransitions(pacmanBoard, pathFinder_pacman.getVertices());
        });

        realRoot.getChildren().addAll(root, slider, start_animation, end_animation);

        Scene scene = new Scene(realRoot);
        scene.getStylesheets().setAll(getClass().getResource("styles.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

    private Board setupPacmanBoard() {
        BoardStructure pacmanBoardStructure = new BoardStructure("src/main/resources/com/gyarmatip/boardapi/pacman_board.txt", PacmanTile.class);
        return new Board(cellSize, pacmanBoardStructure);
    }

    private Board setupHighwayBoard() {
        BoardStructure highwayBoardStructure = new BoardStructure("src/main/resources/com/gyarmatip/boardapi/highway_board.txt", HighwayTile.class);
        return new Board(cellSize, highwayBoardStructure);
    }

    public void initVariables() {
        pacmanBoard = setupPacmanBoard();
        pathFinder_pacman = new DijkstrasPathFinder(pacmanBoard.getBoardStructure(),
                PacmanTile.FOOD, PacmanTile.BIO_FOOD, PacmanTile.PATH);

        highwayBoard = setupHighwayBoard();
        pathFinder_highway = new DijkstrasPathFinder(highwayBoard.getBoardStructure(),
                HighwayTile.STREET, HighwayTile.ZEBRA, HighwayTile.X_CROSS);
    }

}

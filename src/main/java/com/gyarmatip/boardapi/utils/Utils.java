package com.gyarmatip.boardapi.utils;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Utils {

    /**
     * Reads the text content of a file line-by-line into a String array.
     * @param file The file to be read
     * @return An array of the file's lines
     */
    public static String[] readFile(File file) {
        List<String> list = new ArrayList<>();
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNext()) {
                list.add(sc.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
            return null;
        }
        return list.toArray(new String[0]);
    }

    public static Character[][] txtToCharMatrix(File file) {
        char[][] primitive = Arrays
                .stream(Objects.requireNonNull(readFile(file)))
                .map(String::toCharArray)
                .toArray(char[][]::new);

        Character[][] smart = new Character[primitive.length][primitive[0].length];
        for (int i = 0; i < smart.length; i++) {
            for (int j = 0; j < smart[0].length; j++) {
                smart[i][j] = primitive[i][j];
            }
        }
        return smart;
    }

    public static Character[][] txtToCharMatrix(String filePath) {
        return txtToCharMatrix(new File(filePath));
    }

    public static String paintToHex(Paint paint) {
        Color color = ((Color) paint);
        return String.format( "#%02X%02X%02X",
                (int)( color.getRed() * 255 ),
                (int)( color.getGreen() * 255 ),
                (int)( color.getBlue() * 255 ) );
    }

    public static String paintToRGBA(Paint paint) {
        Color color = ((Color) paint);
        return "rgba(" +
                (int)( color.getRed() * 255 ) + ", " +
                (int)( color.getGreen() * 255 ) + ", " +
                (int)( color.getBlue() * 255 ) + ", " +
                (float) color.getOpacity() +
                ")";
    }

}

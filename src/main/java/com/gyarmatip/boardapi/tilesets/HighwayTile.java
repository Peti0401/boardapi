package com.gyarmatip.boardapi.tilesets;

import com.gyarmatip.boardapi.board.tile.TileType;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public enum HighwayTile implements TileType {


    STREET('S', Color.GRAY, null, "", Color.rgb(250, 255, 0, 0.65)),
    ZEBRA('Z', Color.HONEYDEW, null, "", Color.rgb(250, 255, 0, 0.65)),
    PEDESTRIAN_AREA('P', Color.CADETBLUE, null, "", Color.rgb(250, 255, 0, 0.65)),
    GREEN_AREA('G', Color.FORESTGREEN, null, "", Color.rgb(250, 255, 0, 0.65)),
    BUILDING('B', Color.DARKMAGENTA, null, "", Color.rgb(250, 255, 0, 0.65)),
    TREE('T', Color.SADDLEBROWN, null, "", Color.rgb(250, 255, 0, 0.65)),
    RAIL('R', Color.BLACK, null, "", Color.rgb(250, 255, 0, 0.65)),
    CROSS('C', Color.BURLYWOOD, null, "", Color.rgb(250, 255, 0, 0.65)),
    X_CROSS('X', Color.LIGHTSTEELBLUE, null, "", Color.rgb(250, 255, 0, 0.65));

    private final Character character;
    private final Paint basePaint;
    private final String imageURL;
    private final String labelText;
    private final Paint highlighterPaint;

    HighwayTile(Character character, Paint basePaint, String imageURL, String labelText, Paint highlighterPaint) {
        this.character = character;
        this.basePaint = basePaint;
        this.imageURL = imageURL;
        this.labelText = labelText;
        this.highlighterPaint = highlighterPaint;
    }

    public Character getCharacter() {
        return character;
    }

    public Paint getBasePaint() {
        return basePaint;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getLabelText() {
        return labelText;
    }

    public Paint getHighlighterPaint() {
        return highlighterPaint;
    }
}

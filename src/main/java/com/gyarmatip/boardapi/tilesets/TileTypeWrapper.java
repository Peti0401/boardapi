package com.gyarmatip.boardapi.tilesets;

import com.gyarmatip.boardapi.board.tile.TileType;
import javafx.scene.paint.Paint;

public class TileTypeWrapper implements TileType {

    private final Character character;
    private final Paint basePaint;
    private final String imageURL;
    private final String labelText;
    private final Paint highlighterPaint;

    public TileTypeWrapper(Character character, Paint basePaint, String imageURL, String labelText, Paint highlighterPaint) {
        this.character = character;
        this.basePaint = basePaint;
        this.imageURL = imageURL;
        this.labelText = labelText;
        this.highlighterPaint = highlighterPaint;
    }

    @Override
    public Character getCharacter() {
        return character;
    }

    @Override
    public Paint getBasePaint() {
        return basePaint;
    }

    @Override
    public String getImageURL() {
        return imageURL;
    }

    @Override
    public String getLabelText() {
        return labelText;
    }

    @Override
    public Paint getHighlighterPaint() {
        return highlighterPaint;
    }
}
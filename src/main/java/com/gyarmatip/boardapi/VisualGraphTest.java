package com.gyarmatip.boardapi;

import com.gyarmatip.boardapi.graph.Vertex;
import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.swing.mxGraphComponent;
import javafx.application.Application;
import javafx.embed.swing.SwingNode;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.jgrapht.Graph;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.awt.*;

public class VisualGraphTest extends Application {

    private static final Dimension DEFAULT_SIZE = new Dimension(530, 320);

    @Override
    public void start(Stage primaryStage) throws Exception {
        StackPane root = new StackPane();
        SwingNode swingNode = createSwingNode();

        root.getChildren().add(swingNode);

        Scene scene = new Scene(root, 1080, 720);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private SwingNode createSwingNode() {
        Graph<Vertex, DefaultWeightedEdge> g = Main.pathFinder_pacman.getGraph();

        JGraphXAdapter<Vertex, DefaultWeightedEdge> adapter = new JGraphXAdapter<>(g);
        mxGraphComponent component = new mxGraphComponent(adapter);
        component.setConnectable(false);
        component.getGraph().setAllowDanglingEdges(false);

        // positioning via jgraphx layouts
        mxCircleLayout layout = new mxCircleLayout(adapter);

        // center the circle
        int radius = 100;
        layout.setX0((DEFAULT_SIZE.width / 2.0) - radius);
        layout.setY0((DEFAULT_SIZE.height / 2.0) - radius);
        layout.setRadius(radius);
        layout.setMoveCircle(true);

        layout.execute(adapter.getDefaultParent());

        SwingNode swingNode = new SwingNode();
        swingNode.setContent(component);
        return swingNode;
    }

    public static void main(String[] args) {
        launch(args);
    }


}

package com.gyarmatip.boardapi.navigation;

public enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT;
}

package com.gyarmatip.boardapi.navigation.pathfinding;

import com.gyarmatip.boardapi.board.BoardStructure;
import com.gyarmatip.boardapi.board.tile.TileType;
import com.gyarmatip.boardapi.navigation.Position;

import java.util.Arrays;

/**
 * Finds path between 2 given Positions on a Board, represented by a TileType[][].
 */
public abstract class PathFinder {

    /**
     * The structure of the Board
     */
    private BoardStructure boardStructure;

    /**
     * The TileTypes which can be considered as path segments, not obstacles
     */
    private TileType[] allowedTileTypes;

    /**
     * Creates a new instance of {@code PathFinder}.
     * @param boardStructure the Board structure
     * @param allowedTileTypes the TileTypes which can be considered as path segments, not obstacles
     */
    public PathFinder(BoardStructure boardStructure, TileType... allowedTileTypes) {
        this.boardStructure = boardStructure;
        this.allowedTileTypes = allowedTileTypes;
    }

    /**
     * Finds path between two positions.
     * @param startPosition starting position
     * @param endPosition ending position
     * @return an array of Position instance, between startPosition and endPosition, including these endpoints
     */
    public abstract Position[] findPath(Position startPosition, Position endPosition);

    public boolean isAllowedTileType(TileType tileType) {
        return Arrays.asList(allowedTileTypes).contains(tileType);
    }

    public TileType[][] getTileTypeGrid() {
        return boardStructure.getTileTypeGrid();
    }

    public TileType[] getAllowedTileTypes() {
        return allowedTileTypes;
    }

    public int getLowerBoundX() {
        return 0;
    }

    public int getUpperBoundX() {
        return boardStructure.getNumberOfColumns()-1;
    }

    public int getLowerBoundY() {
        return 0;
    }

    public int getUpperBoundY() {
        return boardStructure.getNumberOfRows()-1;
    }
}

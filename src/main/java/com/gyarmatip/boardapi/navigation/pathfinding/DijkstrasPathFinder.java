package com.gyarmatip.boardapi.navigation.pathfinding;

import com.gyarmatip.boardapi.board.BoardStructure;
import com.gyarmatip.boardapi.board.tile.TileType;
import com.gyarmatip.boardapi.graph.Vertex;
import com.gyarmatip.boardapi.navigation.Direction;
import com.gyarmatip.boardapi.navigation.Position;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A PathFinder which implements Dijkstra's algorithm to find the shortest path between two positions
 */
public class DijkstrasPathFinder extends PathFinder {

    private HashSet<Vertex> vertices = new HashSet<>();
    private Graph<Vertex, DefaultWeightedEdge> graph;

    public DijkstrasPathFinder(BoardStructure boardStructure, TileType... allowedTileTypes) {
        super(boardStructure, allowedTileTypes);
        // processing board
        markVertices();
        connectVertices();
        setupGraph();
    }

    /**
     * Marks vertices by analyzing the board's layout
     */
    private void markVertices() {
        TileType[][] tileTypeGrid = getTileTypeGrid();
        for (int r = 0; r < tileTypeGrid.length; r++) {
            for (int c = 0; c < tileTypeGrid[0].length; c++) {
                // Positions of surrounding TileTypes
                Position mid = new Position(c, r);
                Position left = mid.translateWithinBounds(Direction.LEFT, 1, this);
                Position right = mid.translateWithinBounds(Direction.RIGHT, 1,  this);
                Position up = mid.translateWithinBounds(Direction.UP, 1, this);
                Position down = mid.translateWithinBounds(Direction.DOWN, 1, this);

                // number of allowed TileTypes horizontally
                int horizontalSum = Stream
                        .of(left, mid, right)
                        .map(position -> tileTypeGrid[position.getY()][position.getX()])
                        .filter(this::isAllowedTileType)
                        .toArray(TileType[]::new).length;

                // number of allowed TileTypes vertically
                int verticalSum = Stream
                        .of(up, mid, down)
                        .map(position -> tileTypeGrid[position.getY()][position.getX()])
                        .filter(this::isAllowedTileType)
                        .toArray(TileType[]::new).length;

                // vertex found
                if (horizontalSum >= 2 && verticalSum >= 2) {
                    vertices.add(new Vertex(mid));
                }
            }
        }
    }

    /**
     * Sets up connection between adjacent vertices.
     * Two vertices are considered to be connected if they are in the same row or in the same column,
     * and every position between them points to an allowed TileType.
     */
    private void connectVertices() {
        TileType[][] tileTypeGrid = getTileTypeGrid();

        /* Setting up connections horizontally */
        // key: row index
        // value: a list of vertices in that row
        Map<Integer, List<Vertex>> rowMap = vertices.stream().collect(Collectors.groupingBy(Position::getY));
        rowMap.values().forEach(verticesInRow ->
        {
            // sorting by column so that potentially connected vertices are adjacent elements of the list
            verticesInRow.sort(Comparator.comparingInt(Position::getX));

            // traverse all lists
            for (int i = 0; i < verticesInRow.size() ; i++) {
                Vertex current = verticesInRow.get(i);
                int nextIndex = i == verticesInRow.size()-1 ? 0 : (i+1); // handling overlapping connection
                Vertex next = verticesInRow.get(nextIndex);

                // listing in-between positions by translating current to right while the next vertex's position is not reached
                List<Position> inBetweenPositions = current.getPositionInterval(next, Direction.RIGHT, false, true, this);

                // if the in-between positions are pointing to allowed TileTypes only then the vertices are connected
                boolean isConnected = inBetweenPositions.stream()
                        .allMatch(pos -> isAllowedTileType(tileTypeGrid[pos.getY()][pos.getX()]));
                // link vertices bidirectionally and saving weight between them
                if (isConnected) {
                    current.getWeightMap().put(next, inBetweenPositions.size());
                    next.getWeightMap().put(current, inBetweenPositions.size());
                }
            }
        });

        /* Setting up connections vertically */
        // key: column index
        // value: a list of vertices in that column
        Map<Integer, List<Vertex>> columnMap = vertices.stream().collect(Collectors.groupingBy(Position::getX));
        columnMap.values().forEach(verticesInColumn ->
        {
            // sorting by row so that potentially connected vertices are adjacent elements of the list
            verticesInColumn.sort(Comparator.comparingInt(Position::getY));

            // traverse all lists
            for (int i = 0; i < verticesInColumn.size(); i++) {
                Vertex current = verticesInColumn.get(i);
                int nextIndex = i == verticesInColumn.size()-1 ? 0 : (i+1); // handling overlapping connection
                Vertex next = verticesInColumn.get(nextIndex);

                // listing in-between positions by translating current to right while the next vertex's position is not reached
                List<Position> inBetweenPositions = current.getPositionInterval(next, Direction.DOWN, false, true, this);

                // if the in-between positions are pointing to allowed TileTypes only then the vertices are connected
                boolean isConnected = inBetweenPositions.stream()
                        .allMatch(pos -> isAllowedTileType(tileTypeGrid[pos.getY()][pos.getX()]));
                // link vertices bidirectionally and saving weight between them
                if (isConnected) {
                    current.getWeightMap().put(next, inBetweenPositions.size());
                    next.getWeightMap().put(current, inBetweenPositions.size());
                }
            }
        });
    }

    @Override
    public Position[] findPath(Position startPosition, Position endPosition) {
        Optional<Vertex> startVertex = getVertex(startPosition);
        Optional<Vertex> endVertex = getVertex(endPosition);
        if (!startVertex.isPresent() || !endVertex.isPresent()) {
            return null;
        }
        DijkstraShortestPath<Vertex, DefaultWeightedEdge> dijkstraAlg = new DijkstraShortestPath<>(graph);
        ShortestPathAlgorithm.SingleSourcePaths<Vertex, DefaultWeightedEdge> paths =
                dijkstraAlg.getPaths(startVertex.get());
        GraphPath<Vertex, DefaultWeightedEdge> path = paths.getPath(endVertex.get());
        List<Vertex> vertices = path.getVertexList();

        List<Position> positions = new ArrayList<>();
        for (int i = 0; i < vertices.size() - 1; i++) {
            Position cur = vertices.get(i);
            Position next = vertices.get(i+1);
            Direction direction = cur.getDirectionTo(next);
            List<Position> inBetweenPositions = cur.getPositionInterval(next, direction, false, true, this);
            positions.addAll(inBetweenPositions);
        }
        return positions.toArray(new Position[0]);
    }

    /**
     * Converting the set of vertices to actual
     */
    private void setupGraph() {
        graph = new DefaultUndirectedWeightedGraph<>(DefaultWeightedEdge.class);
        vertices.forEach(vertex -> graph.addVertex(vertex));
        graph.vertexSet().forEach(vertex ->
        {
            vertex.getWeightMap().forEach((adjacentVertex, weight) ->
            {
                graph.addEdge(vertex, adjacentVertex);
                graph.setEdgeWeight(vertex, adjacentVertex, weight);
            });
        });
    }

    public Graph<Vertex, DefaultWeightedEdge> getGraph() {
        return graph;
    }

    public HashSet<Vertex> getVertices() {
        return vertices;
    }

    public Optional<Vertex> getVertex(Position position) {
        return vertices.stream()
                .filter(vertex ->
                        vertex.getX() == position.getX() &&
                        vertex.getY() == position.getY())
                .findAny();
    }

    public boolean isVertex(Position position) {
        return getVertex(position).isPresent();
    }

}

package com.gyarmatip.boardapi.navigation.pathfinding;

import com.gyarmatip.boardapi.board.BoardStructure;
import com.gyarmatip.boardapi.board.tile.TileType;
import com.gyarmatip.boardapi.navigation.Position;

/**
 * A PathFinder which implements the A* algorithm to find the shortest path between two positions
 */
public class AStarPathFinder extends PathFinder {

    public AStarPathFinder(BoardStructure boardStructure, TileType... allowedTileTypes) {
        super(boardStructure, allowedTileTypes);
    }

    @Override
    public Position[] findPath(Position startPosition, Position endPosition) {
        return new Position[0];
    }
}

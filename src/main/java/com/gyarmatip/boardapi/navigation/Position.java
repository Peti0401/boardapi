package com.gyarmatip.boardapi.navigation;

import com.gyarmatip.boardapi.navigation.pathfinding.PathFinder;

import java.util.ArrayList;
import java.util.List;

/**
 * A position with integer coordinates only, used to efficiently navigate on 2D array-based boards.
 */
public class Position {

    private int x;
    private int y;

    /**
     * Creates a new instance of {@code Position}.
     * @param x the x coordinate of the position
     * @param y the y coordinate of the position
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Creates a new instance of {@code Position}.
     * @param position an existing position to clone
     */
    public Position(Position position) {
        this(position.x, position.y);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /**
     * Translates this position to a specified direction with a specific translation rate.
     * Translation occurs in a coordinate-system, with the origin being in the top-left corner.
     * <ul>
     *     <li>LEFT: decrease in x-coordinate
     *     <li>RIGHT: increase in x-coordinate
     *     <li>UP: decrease in y-coordinate
     *     <li>DOWN: increase in y-coordinate
     * <ul/>
     * @param direction the direction to which this position gets translated
     * @param rate the rate, the steps with which the position gets translated
     * @return the translated Position
     */
    public Position translate(Direction direction, int rate) {
        Position clone = new Position(this);
        switch (direction) {
            case RIGHT:
                clone.x += rate;
                break;
            case LEFT:
                clone.x -= rate;
                break;
            case DOWN:
                clone.y += rate;
                break;
            case UP:
                clone.y -= rate;
                break;
        }
        return clone;
    }

    /**
     * Translates this position to a specified direction with a specific translation rate, within the specified bounds.
     * If the bound is overstepped on the lower end, then the translated position ends up closer to the upper bound,
     * if the bound is overstepped on the upper end, then the translated position ends up closer to the lower bound.
     * @param direction the direction to which this position gets translated
     * @param rate the rate, the steps with which the position gets translated
     * @param lowerBoundX x-axis (horizontal) inclusive lower bound, non-negative
     * @param upperBoundX x-axis (horizontal) inclusive upper bound, non-negative
     * @param lowerBoundY y-axis (vertical) inclusive lower bound, non-negative
     * @param upperBoundY y-axis (vertical) inclusive upper bound, non-negative
     * @return the translated Position
     */
    private Position translateWithinBounds(Direction direction, int rate,
                                          int lowerBoundX, int upperBoundX,
                                          int lowerBoundY, int upperBoundY) {
        Position clone = new Position(this);
        switch (direction) {
            case RIGHT: // can be out of bounds only at the upper bound of the x-axis
                clone.x += rate;
                if (clone.x > upperBoundX) clone.x = Math.abs((upperBoundX + 1) - Math.abs(clone.x));
                break;
            case LEFT: // can be out of bounds only at the lower bound of the x-axis
                clone.x -= rate;
                if (clone.x < lowerBoundX) clone.x = Math.abs((upperBoundX + 1) - Math.abs(clone.x));
                break;
            case DOWN: // can be out of bounds only at the upper bound of the y-axis
                clone.y += rate;
                if (clone.y > upperBoundY) clone.y = Math.abs((upperBoundY + 1) - Math.abs(clone.y));
                break;
            case UP: // can be out of bounds only at the lower bound of the y-axis
                clone.y -= rate;
                if (clone.y < lowerBoundY) clone.y = Math.abs((upperBoundY + 1) - Math.abs(clone.y));
                break;
        }
        return clone;
    }

    /**
     * Convenience method for bounded translation
     * @param direction the direction to which this position gets translated
     * @param rate the rate, the steps with which the position gets translated
     * @param pathFinder the logic class which encapsulates the boundary values
     * @return
     */
    public Position translateWithinBounds(Direction direction, int rate, PathFinder pathFinder) {
        return translateWithinBounds(direction, rate,
                pathFinder.getLowerBoundX(), pathFinder.getUpperBoundX(),
                pathFinder.getLowerBoundY(), pathFinder.getUpperBoundY());
    }

    /**
     * The supplied endPosition must be in the same row or same column.
     * @param endPosition the position to which the return direction will point from this position
     * @return a direction pointer from this position to endPosition
     */
    public Direction getDirectionTo(Position endPosition) {
        // same column ---> vertical direction
        if (endPosition.getX() == x) {
            return endPosition.getY() > y ? Direction.DOWN : Direction.UP;
        }

        // same row ---> horizontal direction
        if (endPosition.getY() == y) {
            return endPosition.getX() > x ? Direction.RIGHT : Direction.LEFT;
        }

        // not in the same column or in the same row
        return null;
    }

    /**
     * Creates an interval with the lower-bound (exclusive) being this Position and the upper-bound (inclusive) being endPosition.
     * This Position and endPosition must be in the same row or in the same column.
     * @param endPosition upper-bound of the interval
     * @param startDirection the direction in which the stepping starts
     * @param lowerBoundX x-axis (horizontal) inclusive lower bound, non-negative
     * @param upperBoundX x-axis (horizontal) inclusive upper bound, non-negative
     * @param lowerBoundY y-axis (vertical) inclusive lower bound, non-negative
     * @param upperBoundY y-axis (vertical) inclusive upper bound, non-negative
     * @param isLowerInclusive indicates whether the start position (this) should be included
     * @param isUpperInclusive indicates whether the end position should be included
     * @return a list of Positions containing this position and endPosition and all the in-between Positions
     */
    private List<Position> getPositionInterval(Position endPosition, Direction startDirection,
                                               int lowerBoundX, int upperBoundX,
                                               int lowerBoundY, int upperBoundY,
                                               boolean isLowerInclusive, boolean isUpperInclusive) {
        List<Position> positionList = new ArrayList<>();
        if (isLowerInclusive) positionList.add(this);
        Position curPosition = this.translateWithinBounds(startDirection, 1, lowerBoundX, upperBoundX, lowerBoundY, upperBoundY);
        while (!curPosition.equals(endPosition)) {
            positionList.add(curPosition);
            curPosition = curPosition.translateWithinBounds(startDirection, 1, lowerBoundX, upperBoundX, lowerBoundY, upperBoundY);
        }
        if (isUpperInclusive) positionList.add(curPosition);
        return positionList;
    }

    /**
     * Creates an interval with the lower-bound (exclusive) being this Position and the upper-bound (inclusive) being endPosition.
     * This Position and endPosition must be in the same row or in the same column.
     * @param endPosition upper-bound of the interval
     * @param startDirection the direction in which the stepping starts
     * @param isLowerInclusive indicates whether the start position (this) should be included
     * @param isUpperInclusive indicates whether the end position should be included
     * @param pathFinder the logic instance which encapsulates the boundary values
     * @return a list of Positions containing this position and endPosition and all the in-between Positions
     */
    public List<Position> getPositionInterval(Position endPosition, Direction startDirection,
                                               boolean isLowerInclusive, boolean isUpperInclusive,
                                               PathFinder pathFinder) {
        return getPositionInterval(endPosition, startDirection,
                pathFinder.getLowerBoundX(), pathFinder.getUpperBoundX(),
                pathFinder.getLowerBoundY(), pathFinder.getUpperBoundY(),
                isLowerInclusive, isUpperInclusive);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;
        Position position = (Position) o;
        return x == position.x &&
                y == position.y;
    }

    @Override
    public String toString() {
        return "[" + x + ";" + y + "]";
    }

}
